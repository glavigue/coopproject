'use strict'
window.addEventListener('load', function loaded (e) {
    window.removeEventListener('load', loaded)

    const gameArea = document.getElementById('gameArea')
    const timer = document.getElementById('timer')
    const startButton = document.getElementById('startButton')
    const highScore = document.getElementById('highscore')
    const found= document.getElementById('found')
    let idInterval // Variable pour setter l'interval du chronometre

    let turnedCards = [] //Cards qui sont tournes

    const game = (function () {
        /**
         * Function responsable pour creer un element passe en parametre, avec des classes specifiques
         * @param tagName Nom du type du element - tag HTML
         * @param elementClasses Array de string contenant les classes qui seront ajoutees au element cree
         * @returns element HTML du type tagName avec les classes passes par parametre
         */
        function createClassElement (tagName, elementClasses) {
            const element = document.createElement(tagName)
            elementClasses.forEach(elementClass => {
                element.classList.add(elementClass)
            })
            return element
        }

        /**
         *  Fonction qui appelle la methode createClassElement() pour creer une structure de card
         * @returns Returns une div contenant toute la structure d'un card
         */
        function buildCardStructure () {
            const card = createClassElement('div', ['card'])
            const flipContainer = createClassElement('div', ['flipper'])
            const cardBack = createClassElement('div', ['card-back', 'hide'])
            const cardFront = createClassElement('div', ['card-front', 'show'])

            card.appendChild(flipContainer)
            flipContainer.appendChild(cardBack)
            flipContainer.appendChild(cardFront)

            card.addEventListener('click', (e) => {
                turn(cardBack, cardFront)
                turnedCards.push(e.currentTarget)
                checkCards(e.currentTarget)
            })

            return card
        }

        /**
         * Flip le card, et montre son autre cote en faisant un toggle des classes 'hide' et 'show' dans les deux cotes
         * @param {*} cardBack Le dos du card
         * @param {*} cardFront Le front du card
         */
        function turn(cardBack, cardFront){
            cardBack.classList.toggle('show')
            cardBack.classList.toggle('hide')
            cardFront.classList.toggle('show')
            cardFront.classList.toggle('hide')
        }

        /**
         * Methode qui complete la construction d'un card avec les informations prises dans un objet
         * @param {*} cardInfos objet contenant les informations pour contruire un card
         */
        function buildCard (cardInfos, outputArea) {
            const card = buildCardStructure()
            const cardBack = card.children[0].children[0]
            cardBack.style.backgroundImage = `url(${cardInfos.image})`
            card.setAttribute('data-card-id', `${cardInfos.id}`)
            outputArea.appendChild(card)
        }

        function shuffle(array) {
            array.sort(() => Math.random() - 0.5);
        }



        function checkCards(card){
            if(turnedCards.length === 2){
                if(turnedCards[0].getAttribute('data-card-id') === turnedCards[1].getAttribute('data-card-id')){
                    console.log('Equals')
                    found.innerText = parseInt(found.innerText) + 1
                    turnedCards = []
                }

                else{
                    setTimeout(function() {
                        for(let i = 0; i < turnedCards.length; i++){
                            console.log(turnedCards[i])
                            turn(turnedCards[i].children[0].children[0], turnedCards[i].children[0].children[1])
                        }

                        turnedCards.pop()
                        turnedCards.pop()
                    }, 500)



                }


            }
        }

        function checkHighScore(found){

        }

        return {
            start: function (gameArea) {
                found.innerText = 0
                shuffle(CARDS)
                CARDS.forEach(card => {
                    buildCard(card, gameArea)
                })
                shuffle(CARDS)
                CARDS.forEach(card => {
                    buildCard(card, gameArea)
                })

                found.addEventListener('onchange', (e) => {
                    alert('change')
                })
                // generateRandomCards(CARDS)
                startButton.value = 'Stop'
                startTimer(timer)
            },

            stop: function(gameArea){
                while (gameArea.childElementCount > 0) {
                    gameArea.removeChild(gameArea.lastChild)
                }
                highScore.innerText = timer.innerText
                stopTimer(idInterval)
                startButton.value = 'Start'
            }
        }
    })()


    function checkHighScore(score, highScore){

    }

    startButton.addEventListener('click', (e) => {
        if (startButton.value === 'Start') {
            e.preventDefault()
            game.start(gameArea)

        } else if (startButton.value === 'Stop') {
            e.preventDefault()
            game.stop(gameArea)
        }
    })



    function startTimer (timerElement) {
        let time = 0
        idInterval = setInterval(function () {
            time++
            timerElement.innerText = time + 's'
        }, 1000)
    }
    function stopTimer (idInterval) {
        clearInterval(idInterval)

    }
}, false)

// const memoire = (function () {
//     function init () {

//     }
//     return {
//         init: init
//     }
// })()
