'use strict'

const MONTHS = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']

const SALES = [120, 500, 350, 400, 600, 890, 450, 100, 250, 300, 650, 450]

let total = 0 // Total des vente: SALES

function salesSum () {
    for (let index = 0; index < SALES.length; index++) {
        total += SALES[index]
        console.log(total)
    }
}

function showTableVertical () {
    document.write('<table>')
    for (let index = 0; index < MONTHS.length; index++) {
        document.write(
            '<tr>',
            '<td>',
            MONTHS[index],
            '</td>',
            '<td>',
            SALES[index],
            '</td>',
            '</tr>'

        )
    }
    document.write(
        '<th>',
        'Total',
        '</th>',
        '<th>',
        total,
        '</th>'
    )
    document.write('</table>')
}

function showTableHorizontal () {
    document.write('<table>', '<tr>')
    for (let index = 0; index < MONTHS.length; index++) {
        document.write(
            '<td>',
            MONTHS[index],
            '</td>'
        )
    }
    document.write(
        '<th>',
        'Total',
        '</th>'
    )
    document.write('<tr>')
    for (let j = 0; j < SALES.length; j++) {
        document.write(
            '<td>',
            SALES[j],
            '</td>'
        )
    }
    document.write(
        '<th>',
        total,
        '</th>'
    )
    document.write('</tr>')
    document.write('</tr>', '</table>')
}
salesSum()
showTableVertical()
showTableHorizontal()
