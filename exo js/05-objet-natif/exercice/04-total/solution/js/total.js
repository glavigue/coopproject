'use strict'

const MONTHS = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre']

const SALES = [120, 500, 350, 400, 600, 890, 450, 100, 250, 300, 650, 450]

let total = 0 // Total des vente: SALES

// Création d'une table HTML "verticale"
document.write('<h2>Affichage vertical</h2>')
// Création d'une table HTML
document.write('<table>')

// 12 lignes du tableau
for (let i = 0; i < MONTHS.length; i++) {
    document.write('<tr><td>', MONTHS[i], '</td><td>', SALES[i], '</td></tr>')
    total = total + SALES[i] // Autre écriture : total += SALES[i]
}

// "Footer" du tableau
document.write('<tr><th>Total</th><th>', total, ' $</th></tr>')
document.write('</table>')

// Création d'une table HTML horizontale
document.write('<h2>Affichage horizontal</h2>')
document.write('<table>')
document.write('<tr>')
for (let i = 0; i < MONTHS.length; i++) {
    document.write('<td>' + MONTHS[i] + '</td>')
}
document.write('<th>Total</th>')
document.write('</tr>')

// Ligne des montants
document.write('<tr>')
for (let i = 0; i < SALES.length; i++) {
    document.write('<td>', SALES[i], '</td>')
}
document.write('<th>', total, ' $</th>')
document.write('</tr>')

document.write('</table>')
