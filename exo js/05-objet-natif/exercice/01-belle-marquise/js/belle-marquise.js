'use strict'

const VIRGULE = ', '
const tableau = [
    'belle Marquise',
    'vos beaux yeux',
    'me font mourir',
    'd\'amour'
]

function writePoeme () {
    document.write('<ul>')
    document.write('<li>', tableau[0], VIRGULE, tableau[1], VIRGULE, tableau[2], VIRGULE, tableau[3], '</li>')
    document.write('<li>', tableau[1], VIRGULE, tableau[0], VIRGULE, tableau[3], VIRGULE, tableau[2], '</li>')
    document.write('<li>', tableau[3], VIRGULE, tableau[2], VIRGULE, tableau[0], VIRGULE, tableau[1], '</li>')
    document.write('<li>', tableau[1], VIRGULE, tableau[0], VIRGULE, tableau[3], VIRGULE, tableau[1], '</li>')
    document.write('</ul>')
}

writePoeme()
