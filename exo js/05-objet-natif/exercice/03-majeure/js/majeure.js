'use strict'

// Age de la majorité
const MAJOR = 18

// Les ages de 4 groupes de personne
const AGES = [
    [25, 39, 51, 22, 15, 44],
    [25, 39, 51, 22, 55, 44],
    [25],
    [15]
]
const MINEUR = 'ne sont pas tous adultes'
const MAJOR_TEXT = 'sont tous adultes'
// si mon value dans le tableau est > que 18 continue sinon isMineur = true et sort de la boucle
function inspectAge () {
    for (let index = 0; index < AGES.length; index++) {
        let isMineur = false

        const counter = AGES[index].length
        let j = 0
        while (j < counter && !isMineur) {
            if (AGES[index][j] <= MAJOR) {
                console.log('jai trouver un mineur')
                isMineur = true
                console.log(isMineur)
            }
            j++
        }
        // html result
        // code plus lisible quand balise html sont apres la logique
        // html afficher les resultat de la logique
        document.write('<table>')
        document.write('<tr>', '<td>', 'Group ', index)
        if (isMineur === true) {
            // pas tous adulte
            document.write(' ', MINEUR, '</td>', '</tr>')
        }
        // tous adulte
        if (isMineur === false) {
            document.write(' ', MAJOR_TEXT, '</td>', '</tr>')
        }
        document.write('</table>')
    }
}

inspectAge()
