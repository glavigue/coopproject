'use strict'

// Age de la majorité
const MAJOR = 18

// Les ages de 4 groupes de personne
const AGES = [
    [25, 39, 51, 22, 15, 44],
    [25, 39, 51, 22, 55, 44],
    [25],
    [15]
]

AGES.forEach(function (ages, index) {
    let counter = 0
    let result = 'ne sont pas tous adultes'
    const groupLength = AGES[index].length

    while (counter < groupLength && ages[counter] > MAJOR) {
        counter++
    }

    if (counter === groupLength) {
        result = 'sont tous adultes'
    }

    document.writeln('<tr><td>Groupe ' + index + '</td><td>' + result + '</td></tr>')
})

// Meme code avec une boucle for
/* for (let c = 0; c < AGES.length; c++) {
    let counter = 0
    let result = 'ne sont pas tous adultes'
    const groupLength = AGES[c].length

    while (counter < groupLength && AGES[c][counter] > MAJOR) {
        counter++
    }

    if (counter === groupLength) {
        result = 'sont tous adultes'
    }

    document.writeln('<tr><td>Groupe ' + c + '</td><td>' + result + '</td></tr>')
} */
