/**
 * Exercice - tic tac toe
 *
 * Révision portant sur les évènements et le traitement des collections.
 *
 * Consignes:
 *  Créer le fichier js/tic-tac-toe.js.
 *  Compléter le code manquant pour créer un jeu de tic tac toe.
 *      - Un click sur une case place un X et laisse ensuite l'ordinateur jouer son tour.
 *      - L'ordinateur place un O dans un endroit aléatoire en choisissant une case vide.
 *      - Lorsque 3 symboles identiques forment une séquence consécutive, une barre noir apparaît pour indiquer le gagnant.
 */
 'use strict'

 // eslint-disable-next-line no-unused-vars
 const TicTacToe = (function () {
     const X = 'x'
     const O = 'o'

     const gridContainer = document.createElement('div')
     gridContainer.className = 'grid-container'

     function buildWinnerDatas (tileSize) {
         const lineUnit = tileSize / 6

         return [
             {
                 indexs: [0, 1, 2],
                 x: 0,
                 y: lineUnit,
                 angle: 0,
                 width: 600,
                 height: 20
             },
             {
                 indexs: [3, 4, 5],
                 x: 0,
                 y: lineUnit * 3,
                 angle: 0,
                 width: 600,
                 height: 20
             },
             {
                 indexs: [6, 7, 8],
                 x: 0,
                 y: lineUnit * 5,
                 angle: 0,
                 width: 600,
                 height: 20
             },

             {
                 indexs: [0, 3, 6],
                 x: lineUnit,
                 y: 0,
                 angle: 0,
                 width: 20,
                 height: 600
             },
             {
                 indexs: [1, 4, 7],
                 x: lineUnit * 3,
                 y: 0,
                 angle: 0,
                 width: 20,
                 height: 600
             },
             {
                 indexs: [2, 5, 8],
                 x: lineUnit * 5,
                 y: 0,
                 angle: 0,
                 width: 20,
                 height: 600
             },

             {
                 indexs: [0, 4, 8],
                 x: -20,
                 y: lineUnit * 3,
                 angle: 45,
                 width: 650,
                 height: 20
             },
             {
                 indexs: [6, 4, 2],
                 x: -20,
                 y: lineUnit * 3,
                 angle: 135,
                 width: 650,
                 height: 20
             }

         ]
     }

     /**
      * 0|1|2
      * 3|4|5
      * 6|7|8
      *
      * @param {*} tileSize
      */
     function buildGridDatas (tileSize) {
         const tiles = []
         for (let row = 0; row < 3; row++) {
             for (let col = 0; col < 3; col++) {
                 tiles.push({
                     x: col * tileSize,
                     y: row * tileSize,
                     value: ''
                 })
             }
         }
         return tiles
     }

     function init (config) {
         gridContainer.style.width = config.size + 'px'

         const gameMain = document.getElementById(config.id)
         gameMain.appendChild(gridContainer)

         const tileSize = config.size / 3

         const gridDatas = buildGridDatas(tileSize)

         gridDatas.forEach((data, index) => {
             const cellDiv = document.createElement('div')
             cellDiv.addEventListener('click', function (event) {
                 // code pour placer le symbole du joueur
             })
             cellDiv.style.width = tileSize + 'px'
             cellDiv.style.height = tileSize + 'px'
             cellDiv.style.left = data.x + 'px'
             cellDiv.style.top = data.y + 'px'
             gridContainer.appendChild(cellDiv)
         })
     }

     return {
         init: init
     }
 })()
