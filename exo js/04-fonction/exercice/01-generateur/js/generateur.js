'use strict'

function createItem (itemName) {
    return '<li>' + itemName + '</li>'
}

function createList () {
    let toRet = '<ul>'
    for (let index = 0; index < arguments[0].length; index++) {
        toRet += createItem(arguments[0][index])
    }
    toRet += '</ul>'
    return toRet
}
