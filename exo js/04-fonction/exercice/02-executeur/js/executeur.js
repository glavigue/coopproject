'use strict'

function execute (operation, value1, value2) {
    let toReturn
    switch (operation) {
    case 'ADD':
        toReturn = value1 + value2
        break
    case 'SUB':
        toReturn = value1 - value2
        break
    case 'MUL':
        toReturn = value1 * value2
        break
    default:
        // si autre chose cest quoi le type
        return typeof operation === 'function'
            ? operation(value1, value2)
            : typeof operation === 'string'
                ? "Nom d'operation inconnue"
                : "Type d'operation inconnue"
    }
    return toReturn
}
