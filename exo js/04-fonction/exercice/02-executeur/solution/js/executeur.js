'use strict'

// eslint-disable-next-line no-unused-vars
function execute (operation, val1, val2) {
    let result = 0

    function add (a, b) {
        return a + b
    }

    function sub (a, b) {
        return a - b
    }

    function mul (a, b) {
        return a * b
    }

    if (typeof operation === 'string') {
        switch (operation) {
        case 'ADD':
            result = add(val1, val2)
            break

        case 'SUB':
            result = sub(val1, val2)
            break

        case 'MUL':
            result = mul(val1, val2)
            break
        default:
            result = 'Nom d\'opération inconnue'
        }
    } else if (typeof operation === 'function') {
        result = operation(val1, val2)
    } else {
        result = 'Type d\'opération inconnue'
    }

    return result
}
