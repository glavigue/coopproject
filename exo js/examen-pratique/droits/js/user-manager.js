const UserManager = (function () {
    'use strict'

    function init (userRole) {
        const ulElement = document.getElementById('roleList')

        const users = userRole.users
        const roles = userRole.roles
        showUser(users)
        showRoles(roles)

        const tbody = document.querySelector('tbody')
        let isOver = false
        tbody.addEventListener('mouseover', function (event) {
            isOver = true

            selectRole(ulElement, isOver)
            isOver = false
        })
    }

    // id est un string
    // avec lid du user trouver si il contient un des roles afficher
    // comparer si lutilisateur a le role a la list
    // si il a le role faire un toggle qui ajoute la class .selected
    // dont quand lutilisateur va enlever sa souris du tableau la class css va senleve de meme
    // mouseover un element td recherche quelle role user posede et active le toggle pour ajouter le css
    // quand le mouseover ne focus pu aucun td le toggle enleve donc la class css
    function selectRole (ulElement, isOver) {
        const liElements = ulElement.children
        if (isOver === true) {
            for (let index = 0; index < liElements.length; index++) {
                liElements[index].classList.toggle('selected')
            }
        }
    }

    function showUser (users) {
        const tbodyElement = document.querySelector('tbody')

        users.forEach(user => {
            // pour chaque user je dois creer un tr et 4 td
            const tr = createTrElement()
            tr.appendChild(createTdElement(user.id, user.id))
            tr.appendChild(createTdElement(user.userName, user.id))
            tr.appendChild(createTdElement(user.firstName, user.id))
            tr.appendChild(createTdElement(user.lastName, user.id))
            tbodyElement.appendChild(tr)
        })
    }
    // function creer un element Td avec la property en parametre  et lui set un Attribute id
    function createTdElement (property, id) {
        const td = document.createElement('td')
        td.innerText = property
        td.setAttribute('id', id)
        return td
    }

    // function cree un tr eleement
    function createTrElement () {
        const trElement = document.createElement('tr')
        return trElement
    }

    function showRoles (roles) {
        const ulElement = document.getElementById('roleList')
        roles.forEach(role => {
            const temps = createLiElement(role)
            console.log(temps.getAttribute('value'))
            ulElement.appendChild(createLiElement(role))
        })
    }

    function createLiElement (role) {
        const li = document.createElement('li')
        li.innerText = role.label
        li.setAttribute('value', role.code)
        return li
    }

    return {
        init: init
    }
})()
