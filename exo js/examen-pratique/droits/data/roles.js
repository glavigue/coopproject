
// eslint-disable-next-line no-unused-vars
const roles = [
    {
        code: 'admin',
        label: 'Administrateur'
    },
    {
        code: 'user',
        label: 'Utilisateur'
    },
    {
        code: 'support',
        label: 'Support technique'
    },
    {
        code: 'seller',
        label: 'Vendeur'
    }
]
