'use strict'

const SENTENCE = 'The strength of JavaScript is that you can do anything. The weakness is that you will.'
console.log('La phrase entiere: ' + SENTENCE)
console.log('La longueur de la chaine de caractere: ' + SENTENCE.length)
console.log('La position du mot JavaScript: ' + SENTENCE.search('JavaScript'))
console.log("La position du dernier 'is': " + SENTENCE.lastIndexOf('is'))
console.log('La derniere phrase seulement: ' + SENTENCE.split('.'))
console.log('Les 10 caractere a partir du 17 iem caractere: ' + SENTENCE.substr((17 - 1), 10))
console.log('Combient de mots dans la phrase: ' + SENTENCE.split(' ').length)
console.log('Le 61 iem caractere: ' + SENTENCE.charAt(61 - 1))
console.log("Remplacer 'you' par 'I' dans toute la phrase: " + SENTENCE.replaceAll('you', 'I'))
