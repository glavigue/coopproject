'use strict'

const SENTENCE = 'The strength of JavaScript is that you can do anything. The weakness is that you will.'

console.log('La phase entière:', SENTENCE)

console.log('La longueur de la chaine de caractère:', SENTENCE.length)

console.log('La position du mot JavaScript:', SENTENCE.indexOf('JavaScript'))

console.log('La position du dernier \'is\':', SENTENCE.lastIndexOf('is'))

console.log('La dernière phrase seulement:', SENTENCE.slice(SENTENCE.indexOf('.') + 2, SENTENCE.length))

console.log('Les 10 caractère a partir du 17 iem caractère:', SENTENCE.substr(16, 10))

console.log('Combient de mots dans la phrase:', SENTENCE.split(' ').length)

console.log('Le 61 iem caractère:', SENTENCE.charAt(60))

console.log('Remplacer \'you\' par \'I\' dans toute la phrase:', SENTENCE.replace(/you/g, 'I'))
