/**
 * Exercice - chaine-caractere
 *
 * Utiliser les fonctions JavaScript pour le traitement des chaines de caractères.
 *
 * Consignes:
 *  Créer le fichier js/chaine-caractere.js à partir du code suivant.
 *  Compléter le code manquant pour obtenir le résultat de la maquette.
 *
 * Note: Seulement l'objet console est nécessaire, pas d'itération ou autre structure de controle
 */
'use strict'

const SENTENCE = 'The strength of JavaScript is that you can do anything. The weakness is that you will.'
