'use strict'

const temperature = 21.3
const fruits = ['pomme', 'poire', 'orange']

console.log('Salut.')
console.log(temperature)
console.log(fruits)
console.log('La temperature est ', temperature)
console.log('La collection de fruits est ', fruits)
console.log("<h1>Pas d'interpreteur HTML</h1>")
const number = 18
console.log(number, " 'dis-huit' ", true, null, undefined)
console.group('Ceci est un groupe a part')
console.log('Contenu du groupe')
console.groupEnd()
console.groupCollapsed('Ceci est un second groupe a part (ferme par defaut)')
console.log('Contenu du second groupe')
console.groupEnd()
