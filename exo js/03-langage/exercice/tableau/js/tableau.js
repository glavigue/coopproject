'use strict'

const items = ['premier', 'deuxième', 'troisième', 'quatrième']

console.log('=== Exercice 01 ===')
console.log('Le tableau contient ' + items.length + ' elements')

console.log('=== Exercice 02 ===')
console.log(items)

console.log('=== Exercice 03 ===')
for (let index = 0; index < items.length; index++) {
    console.log(items[index])
}

console.log('=== Exercice 04 ===')
const reverseArray = items.reverse()
for (let index = 0; index < reverseArray.length; index++) {
    console.log(reverseArray[index])
}
