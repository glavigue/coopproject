/**
 * Exercice - tableau
 *
 * Objectif:
 *  Révision des variables tableaux.
 *
 * Consignes:
 *  Créer le fichier js/tableau.js à partir du code suivant.
 *  Compléter le code manquant pour obtenir le résultat de la maquette.
 */
'use strict'

const items = ['premier', 'deuxième', 'troisième', 'quatrième']

console.log('=== Exercice 01 ===')

console.log('Le tableau contient', items.length, 'éléments.')

console.log('=== Exercice 02 ===')

console.log(items)

console.log('=== Exercice 03 ===')

for (let counter = 0; counter < items.length; counter++) {
    console.log(items[counter])
}

console.log('=== Exercice 04 ===')

// première solution
for (let counter = items.length - 1; counter >= 0; counter--) {
    console.log(items[counter])
}

// seconde solution
let counter = items.length - 1
while (counter >= 0) {
    console.log(items[counter])
    counter--
}
