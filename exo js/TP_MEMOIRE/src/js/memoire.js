'use strict'
window.addEventListener('load', function loaded (e) {
    window.removeEventListener('load', loaded)

    const gameArea = document.getElementById('gameArea')
    const timer = document.getElementById('timer')
    const startButton = document.getElementById('startButton')
    const highScore = document.getElementById('highscore')

    const game = (function () {
        /**
         * Function responsable pour creer un element passe en parametre, avec des classes specifiques
         * @param tagName Nom du type du element - tag HTML
         * @param elementClasses Array de string contenant les classes qui seront ajoutees au element cree
         * @returns element HTML du type tagName avec les classes passes par parametre
         */
        function createClassElement (tagName, elementClasses) {
            const element = document.createElement(tagName)
            elementClasses.forEach(elementClass => {
                element.classList.add(elementClass)
            })
            return element
        }

        /**
         *  Fonction qui appelle la methode createClassElement() pour creer une structure de card
         * @returns Returns une div contenant toute la structure d'un card
         */
        function buildCardStructure () {
            const card = createClassElement('div', ['card'])
            const flipContainer = createClassElement('div', ['flipper'])
            const cardBack = createClassElement('div', ['card-back', 'hide'])
            const cardFront = createClassElement('div', ['card-front', 'show'])

            card.appendChild(flipContainer)
            flipContainer.appendChild(cardBack)
            flipContainer.appendChild(cardFront)

            card.addEventListener('click', (e) => {
                cardBack.classList.toggle('show')
                cardBack.classList.toggle('hide')
                cardFront.classList.toggle('show')
                cardFront.classList.toggle('hide')
            })

            return card
        }

        /**
         * Methode qui complete la construction d'un card avec les informations prises dans un objet
         * @param {*} cardInfos objet contenant les informations pour contruire un card
         */
        function buildCard (cardInfos, outputArea) {
            const card = buildCardStructure()
            const cardBack = card.children[0].children[0]
            cardBack.style.backgroundImage = `url(${cardInfos.image})`
            card.setAttribute('id', `card${cardInfos.id}`)
            outputArea.appendChild(card)
        }
        /**
         *return highscore selon le temps ecouler pour trouver les cartes
         * @returns highscore
         */
        function computeHighscore (time) {
            console.log('took you this much time:' time, 'score before', highScore.value )
            if (highScore !== 0) {
                highScore.value = time
            }
            return highScore
        }

        return {
            start: function (gameArea) {
                CARDS.forEach(card => {
                    buildCard(card, gameArea)
                })
                startButton.value = 'Stop'
                startTimer(timer)
            },

            stop: function (gameArea) {
                while (gameArea.childElementCount > 0) {
                    gameArea.removeChild(gameArea.lastChild)
                }
                stopTimer(idInterval)
                computeHighscore(timer.value)
                startButton.value = 'Start'
            }
        }
    })()

    startButton.addEventListener('click', (e) => {
        if (startButton.value === 'Start') {
            e.preventDefault()
            game.start(gameArea)
        } else if (startButton.value === 'Stop') {
            e.preventDefault()
            game.stop(gameArea)
        }
    })

    let idInterval

    function startTimer (timerElement) {
        let time = 0
        idInterval = setInterval(function () {
            time++
            timerElement.value = time
            console.log('hello', time)
        }, 1000)
    }
    function stopTimer (idInterval) {
        clearInterval(idInterval)
    }
}, false)

// const memoire = (function () {
//     function init () {

//     }
//     return {
//         init: init
//     }
// })()
