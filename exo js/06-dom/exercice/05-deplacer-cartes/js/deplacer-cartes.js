/**
 * Exercice - deplaer-cartes
 *
 * Consignes:
 *  Créer le fichier js/deplacer-cartes.js à partir du code suivant.
 *  Compléter le code manquant pour obtenir le résultat des maquettes.
 *  Ne pas modifier le code HTML
 */
'use strict'

// get le conteneur de carte 'cartes'
// definir tournerDroite
// definir tournerGauche

const carteContainer = document.getElementById('cartes')

function tournerDroite () {
    // optenir le firstchild afin de deplacer la carte a droite
    carteContainer.appendChild(carteContainer.firstElementChild)
}

function tournerGauche () {
    // optenir le lastchild et lassigner a la place du firstchild
    carteContainer.appendChild(carteContainer.lastElementChild.nextSibling)
}
