/**
 * Exercice - todo-list
 *
 * Consignes:
 *  Créer le fichier js/todo-list.js à partir du code suivant.
 *  Compléter le code manquant pour obtenir le résultat des maquettes.
 *  Ne pas modifier le code HTML
 *
 *  Note: La console ne doit pas afficher de message d'erreur lors de la suppression si la liste est vide.
 */
'use strict'

// get input de lutilisateur textBox
// definir la methode addNewLiElement qui ajoute le text du textbox dans une list html
// definir la methode deleteLastLiElement qui supprime le text de mon array
// si array est vide et quon appuy sur supprimer gerer erreur

const inputContent = document.getElementById('textBox')
const ulList = document.getElementById('liste')

// eslint-disable-next-line no-unused-vars
function addNewLiElement () {
    // creer des li pour chaque todo
    const li = document.createElement('li')

    const textNode = document.createTextNode(inputContent.value)
    li.appendChild(textNode)
    ulList.appendChild(li)
}

// eslint-disable-next-line no-unused-vars
function deleteLastLiElement () {
    // delete lastchild
    const lastLi = ulList.removeChild(ulList.lastChild)
    if (lastLi === null) {
        console.log('todo list vide')
    }
}
