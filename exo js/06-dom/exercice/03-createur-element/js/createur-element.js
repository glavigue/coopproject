/**
 * Exercice - createur-element
 *
 * Consignes:
 *  Créer le fichier js/createur-element.js à partir du code suivant.
 *  Compléter le code manquant pour l'exécution des 2 boutons qui permettent de crée des élément dans la balise div avec un id="output"
 *      - Appliquer la class inner-html pour les éléments crée avec innerHTML.
 *      - Appliquer la class create-element pour les éléments crée avec createElement
 *  Ne pas modifier le code HTML
 */

'use strict'

// recuperer elementType choisi
// recuperer elementContent utilisateur a ecrit
// definir les methodes createWithInnerHTML et createWithCreateElement
// afficher le resultat dans la div output

const selectOption = document.getElementById('elementType')
const elementContent = document.getElementById('elementContent')
const output = document.getElementById('output')

// eslint-disable-next-line no-unused-vars
function createWithInnerHTML () {
    const elementHtml = document.createElement(selectOption.options[selectOption.selectedIndex].text)
    elementHtml.innerHTML = elementContent.value
    elementHtml.classList.add('inner-html')
    output.appendChild(elementHtml)
}

// eslint-disable-next-line no-unused-vars
function createWithCreateElement () {
    const option = selectOption.options[selectOption.selectedIndex].text

    const elementToCreate = document.createElement(option)
    const textNode = document.createTextNode(elementContent.value)
    elementToCreate.classList.add('create-element')
    elementToCreate.appendChild(textNode)

    output.appendChild(elementToCreate)
}
