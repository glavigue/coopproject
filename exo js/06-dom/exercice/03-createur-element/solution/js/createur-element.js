'use strict'

const elementType = document.getElementById('elementType')

const elementContent = document.getElementById('elementContent')

const outputDiv = document.getElementById('output')

// eslint-disable-next-line no-unused-vars
function createWithInnerHTML () {
    outputDiv.innerHTML += '<' + elementType.value + ' class="inner-html">' + elementContent.value + '</' + elementType.value + '>'
}

// eslint-disable-next-line no-unused-vars
function createWithCreateElement () {
    const newElement = document.createElement(elementType.value)
    newElement.classList.add('create-element')
    outputDiv.appendChild(newElement)

    const newText = document.createTextNode(elementContent.value)
    newElement.appendChild(newText)
}
