'use strict'

const billes = document.getElementsByClassName('bille')

billes[0].classList.add('arrondi')

billes[1].classList.add('orange')
billes[1].classList.add('taille-x')

billes[2].classList.add('taille-xx')
billes[2].classList.add('incline')

billes[3].classList.add('mauve')
billes[3].classList.add('arrondi')
billes[3].classList.add('decale')

billes[4].classList.add('taille-xxx')
billes[4].classList.add('arrondi')
billes[4].classList.add('mauve')
