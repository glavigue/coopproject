'use strict'

// get all the element in the container

/** for each element ajouter toute les classe css qui aura besoin
*pour avoir le meme resultat que la maquette
*/

/**
 * bille[0] arrondi
 * bille[1] orange taille-x
 * bille[2] taille-xx incline
 * bille[3] mauve decale
 * bille[4] mauve taille-xxx
 */

// quel est la difference entre queryselector et getelementbyid
// const divContainer = document.querySelector('container')
const test = document.getElementById('container')
const tableauBille = test.children
console.log(tableauBille)
console.log(test.children[0])

tableauBille[0].classList.add('arrondi')
tableauBille[1].classList.add('orange', 'taille-x')
tableauBille[2].classList.add('taille-xx', 'incline')
tableauBille[3].classList.add('mauve', 'decale', 'arrondi')
tableauBille[4].classList.add('mauve', 'taille-xxx', 'arrondi')
