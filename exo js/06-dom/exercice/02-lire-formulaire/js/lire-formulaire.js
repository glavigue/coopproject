'use strict'

/**
 * get les inputs nom prenom
 * declarer la fonction readform
 * -createElement et creer son textnode pour ensuite append
 */
// recherche du id une seule fois necessaire vue que le id ne changera pas
const output = document.getElementById('output')
const lastNameHolder = document.getElementById('lastName')
const firstNameHolder = document.getElementById('firstName')

function readForm () {
    const pElementName = document.createElement('p')
    const pElementFirstName = document.createElement('p')

    const textNodeName = document.createTextNode(lastNameHolder.value)
    pElementName.appendChild(textNodeName)
    output.appendChild(pElementName)

    const textNodeFirstName = document.createTextNode(firstNameHolder.value)
    pElementFirstName.appendChild(textNodeFirstName)
    output.appendChild(pElementFirstName)
}
