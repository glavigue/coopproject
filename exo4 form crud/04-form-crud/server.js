'use strict'

const express = require('express')

const app = express()

app.use(express.urlencoded({ extended: true }))

app.use(express.json())

app.use(express.static('dist'))

// CORS for development
// https://enable-cors.org/server_expressjs.html
app.use(function (request, response, next) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Credentials', 'false')
    next()
})

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const USERS = [
    {
        id: 1,
        userName: 'patate',
        firstName: 'Pat',
        lastName: 'Ate'
    },
    {
        id: 2,
        userName: 'gcote',
        firstName: 'Gros',
        lastName: 'Coté'
    },
    {
        id: 3,
        userName: 'fmartineau',
        firstName: 'François',
        lastName: 'Martineau'
    },
    {
        id: 4,
        userName: 'mstpierre',
        firstName: 'Marc',
        lastName: 'St-Pierre'
    },
    {
        id: 5,
        userName: 'msimard',
        firstName: 'Mélanie',
        lastName: 'Simard'
    },
    {
        id: 6,
        userName: 'agermain',
        firstName: 'Audrée',
        lastName: 'Germain'
    }
]

// La valeur du prochain ID pour la création d'un objet à ajouter dans la collection.
const nextId = USERS.length + 1
function getUserId (user, request) {
    console.log(user.id, 'la request', typeof request.params.id)
    if (user.id == request.params.id) {
        return user
    } else {
        return console.log('user not found')
    }
}
app.get('/users', function (request, response) {
    const usersJSON = JSON.stringify(USERS, null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(usersJSON)
})

app.get('/users/:id', function (request, response) {
    // const user = USERS.map((user) => getUserId(user, request))
    const user = USERS.find((user) => getUserId(user, request))
    const userJSON = JSON.stringify(user)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end(userJSON)
})

app.post('/user', function (request, response) {
    const user = {
        id: nextId,
        userName: request.body.userName,
        firstName: request.body.firstName,
        lastName: request.body.lastName
    }
    USERS.push(user)
    const testObjectString = JSON.stringify(USERS, null, 4)
    console.log(testObjectString)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(testObjectString)
})

app.put('/user', function (request, response) {
    const user = USERS.find((user) => (user.id == request.body.id) ? user : console.log('user not found'))
    user.lastName = request.body.lastName
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(user))
})

app.delete('/user/:id', function (request, response) {
    const user = USERS.findIndex((user) => (user.id == request.params.id) ? user : console.log('user not found'))
    USERS.splice(user, 1)

    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(USERS))
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
