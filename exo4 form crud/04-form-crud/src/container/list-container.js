import React, { Component } from 'react'

import ListItemComponent from '../component/list-item-component'

class ListContainer extends Component {
    constructor () {
        super()

        this.state = {
            users: []
        }
        this.handleDeleteOnClick = this.handleDeleteOnClick.bind(this)
    }

    componentDidMount () {
        fetch('/users', { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    handleDeleteOnClick (e) {
        console.log(e.target.value)
        this.setState({ users: [1] })
    }

    render () {
        console.log(this.state.users)

        return (
            <div>
                <h1>Liste d'usager</h1>
                <ul>
                    {this.state.users.map((user, index) => <ListItemComponent key={index} text={user.userName} onHandleDelete={this.handleDeleteOnClick} />)}
                </ul>
                <button type='button' onClick={this.props.onHandleAddOnClick}>Ajouter</button>
            </div>
        )
    }
}

export default ListContainer
