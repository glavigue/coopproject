import React, { Component } from 'react'

import FormContainer from './form-container'
import ListContainer from './list-container'

class ApplicationContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            // Les champs du formulaire
            formValues: {},
            // Indique si le formulaire doit être affiché
            showForm: false,
            // Collection d'objet affiché liste
            users: []
        }
        this.handleAddOnClick = this.handleAddOnClick.bind(this)
        this.handleSaveOnClick = this.handleSaveOnClick.bind(this)
    }

    handleAddOnClick () {
        this.setState({ showForm: true })
    }

    handleSaveOnClick () {
        this.setState({ showForm: false })
    }

    render () {
        return (
            <div>
                {this.state.showForm ? <FormContainer onHandleSaveOnClick={this.handleSaveOnClick} /> : <ListContainer onHandleAddOnClick={this.handleAddOnClick} />}
            </div>
        )
    }
}
export default ApplicationContainer
