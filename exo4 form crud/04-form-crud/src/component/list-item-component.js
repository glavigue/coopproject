import React from 'react'

const ListItemComponent = ({ text, onHandleDelete }) => (
    <li>{text}<button type='button' onClick={onHandleDelete}>X</button></li>
)

export default ListItemComponent
